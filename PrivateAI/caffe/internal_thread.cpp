#include "caffe/internal_thread.hpp"
#include "caffe/util/math_functions.hpp"

namespace caffe {

int InternalThread::totalThread = 0;
vector<InternalThread*> InternalThread::_thread;
sgx_thread_mutex_t InternalThread::mutex = SGX_THREAD_MUTEX_INITIALIZER;

InternalThread::InternalThread()
{
	tid = -1;
	should_stop = false;
	sgx_thread_mutex_lock(&mutex);
	index = _thread.size();
	_thread.push_back(this);
	sgx_thread_mutex_unlock(&mutex);
}

InternalThread::~InternalThread() {
  StopInternalThread();
}

bool InternalThread::is_started() const {
  return tid != -1;
}

bool InternalThread::must_stop() {
  return should_stop;
}

void InternalThread::StartInternalThread() {
	if (is_started())
	{
		LOG(WARNING) << "Already started!";
		return;
	}
	sgx_thread_mutex_lock(&mutex);
	if (totalThread == maxTotalThread)
	{
		LOG(FATAL) << "No more cpu core";
		return;
	}
	++totalThread;
	sgx_thread_mutex_unlock(&mutex);
	ocall_newInternalThread(&index);
}

void InternalThread::entry(int device, Caffe::Brew mode, int rand_seed,
    int solver_count, int solver_rank, bool multiprocess) {

  Caffe::set_mode(mode);
  Caffe::set_random_seed(rand_seed);
  Caffe::set_solver_count(solver_count);
  Caffe::set_solver_rank(solver_rank);
  Caffe::set_multiprocess(multiprocess);

  InternalThreadEntry();
}

void InternalThread::StopInternalThread() {
  if (is_started()) {
	sgx_thread_mutex_lock(&mutex);
	--totalThread;
	sgx_thread_mutex_unlock(&mutex);
	should_stop = true;
  }
}

}  // namespace caffe


void ecall_newInternalThread(int* index)
{
	int device = 0;
	caffe::Caffe::Brew mode = caffe::Caffe::mode();
	int rand_seed = caffe::caffe_rng_rand();
	int solver_count = caffe::Caffe::solver_count();
	int solver_rank = caffe::Caffe::solver_rank();
	bool multiprocess = caffe::Caffe::multiprocess();

	caffe::InternalThread::_thread[*index]->entry(device, mode, rand_seed, solver_count, solver_rank, multiprocess);
}
