#include "myLib/myFile.hpp"

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/text_format.h>

#include "caffe/common.hpp"
#include "caffe/proto/caffe.pb.h"
#include "caffe/util/io.hpp"

const int kProtoReadBytesLimit = INT_MAX;  // Max size of 2 GB minus 1 byte.

namespace caffe {

	using google::protobuf::io::FileInputStream;
	using google::protobuf::io::ZeroCopyInputStream;
	using google::protobuf::io::CodedInputStream;
	using google::protobuf::Message;

	bool ReadProtoFromTextFile(const char* filename, Message* proto) {
		int fd = open(filename, O_RDONLY);
		CHECK_NE(fd, -1) << "File not found: " << filename;
		FileInputStream* input = new FileInputStream(fd);
		bool success = google::protobuf::TextFormat::Parse(input, proto);
		delete input;
		close(fd);
		return success;
	}

	void WriteProtoToTextFile(const Message& proto, const char* filename) {
	}

	bool ReadProtoFromBinaryFile(const char* filename, Message* proto) {
#if defined (_MSC_VER)  // for MSC compiler binary flag needs to be specified
		int fd = open(filename, O_RDONLY | O_BINARY);
#else
		int fd = open(filename, O_RDONLY);
#endif
		CHECK_NE(fd, -1) << "File not found: " << filename;
		ZeroCopyInputStream* raw_input = new FileInputStream(fd);
		CodedInputStream* coded_input = new CodedInputStream(raw_input);
		coded_input->SetTotalBytesLimit(kProtoReadBytesLimit, 536870912);

		bool success = proto->ParseFromCodedStream(coded_input);

		delete coded_input;
		delete raw_input;
		close(fd);
		return success;
	}

	void WriteProtoToBinaryFile(const Message& proto, const char* filename) {
	}

	static bool matchExt(const std::string & fn,
		std::string en) {
		size_t p = fn.rfind('.');
		std::string ext = p != fn.npos ? fn.substr(p) : fn;
		std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
		std::transform(en.begin(), en.end(), en.begin(), ::tolower);
		if (ext == en)
			return true;
		if (en == "jpg" && ext == "jpeg")
			return true;
		return false;
	}

	bool ReadFileToDatum(const string& filename, const int label,
		Datum* datum) {

		int fd = open(filename.c_str(), O_RDONLY);
		if (fd == -1) 
			return false;

		int size = datum->channels() * datum->height() * datum->width();
		if (size != file_pool[fd].size) return false;
		std::string buffer(size, ' ');
		myRead(fd, &buffer[0], size);
		close(fd);

		datum->set_data(buffer);//data_->assign(buffer)
		datum->set_label(label);//label_ = label
		datum->set_encoded(true);
		return true;
	}

	bool ReadImageToDatum(const string& filename, const int label,
		const int height, const int width, const bool is_color,
		const std::string & encoding, Datum* datum)
	{
		//Datum datum_;
		//ocall_ReadImageToDatum(filename.c_str(), label, height, width, (int)is_color, encoding.c_str(), (long long*)&datum_);
		//*datum = datum_;
		//return true;
		ocall_PrepareBinaryImage(filename.c_str(), height, width, (int)is_color, encoding.c_str());
		if (encoding.size() && !matchExt(filename, encoding))
			return false;
		datum->set_channels(is_color? 3 : 1);
		datum->set_height(height);
		datum->set_width(width);
		datum->clear_data();
		datum->clear_float_data();
		datum->set_encoded(false);
		return ReadFileToDatum(filename + ".binary", label, datum);
	}

}