#include "myLib/myFile.hpp"
#include "myLib/myGlog.hpp"
//#include "myLib/myMemory.hpp"

FILE* stdin;
FILE* stdout;
FILE* stderr;

unordered_map<int, file_t> file_pool;

myStream myLog[NUM_LEVEL];
myStream myDLog[NUM_LEVEL];
myStream myCheck;
myStream myDCheck;
