#include "myLib/myMemory.hpp"
#include <memory>
#include <vector>
#include "PrivateAI_t.h"

//#define MY_MEMORY_VERBOSE

int mem_flag = ENCLAVE_ALL;
int mymem_total_size = 0;

int get_memory_flag() {
	return mem_flag;
}

void set_memory_flag(int flag) {
	mem_flag = flag;
}

void* myMalloc(size_t size) {
	void* new_ptr;
	if (mem_flag > OCALL_MEM_ONLY) {
#ifdef MY_MEMORY_VERBOSE
		ocall_print("     [MY MEM] E malloc ");
		ocall_print_int(size);
		ocall_print(" / ");
#endif		
		int* new_ptr_ = (int *)malloc(size + sizeof(size));
		if (new_ptr_ == 0 || new_ptr_ == NULL)
			ocall_print("\n\nMalloc ERROR\n\n");
		*new_ptr_ = size;
		new_ptr = (void*)(new_ptr_ + 1);

		mymem_total_size += size;
#ifdef MY_MEMORY_VERBOSE
		ocall_print_int(mymem_total_size);
		ocall_print("\t(ptr: ");
		ocall_print_ptr((long long)new_ptr);
		ocall_print(")\n");
#endif
	}
	else {

		ocall_malloc(size, (long long *)&new_ptr);
	}
	return new_ptr;
}
void myFree(void* ptr)
{
	if (mem_flag > OCALL_MEM_ONLY)
	{
		int* ptr_ = ((int*)ptr) - 1;
		int size = *ptr_;
#ifdef MY_MEMORY_VERBOSE
		ocall_print("\tmyFree ");
		ocall_print_int(size);
		ocall_print("\t(ptr: ");
		ocall_print_ptr((long long)ptr);
		ocall_print(")\n");
#endif
		free((void*)ptr_); 
		mymem_total_size -= size;
	}
	else {
		ocall_free((long long)ptr);
	}
}

void myMemorySize(void *ptr) {
	if (mem_flag == OCALL_MEM_ONLY)
		return;
	int* ptr_ = ((int*)ptr) - 1;
	int size = *ptr_;
	ocall_print("\tmyMemorySize ");
	ocall_print_ptr((long long)ptr);
	ocall_print(" - ");
	ocall_print_int(size);
	ocall_print("\n");
}
