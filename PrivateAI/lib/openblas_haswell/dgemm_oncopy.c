#define DOUBLE
#define ASMNAME _dgemm_oncopy
#define ASMFNAME _dgemm_oncopy_
#define NAME dgemm_oncopy_
#define CNAME dgemm_oncopy
#define CHAR_NAME "dgemm_oncopy_"
#define CHAR_CNAME "dgemm_oncopy"
#define DOUBLE
#include "kernel/generic/gemm_ncopy_2.c"