#define ASMNAME _dgemv_n
#define ASMFNAME _dgemv_n_
#define NAME dgemv_n_
#define CNAME dgemv_n
#define CHAR_NAME "dgemv_n_"
#define CHAR_CNAME "dgemv_n"
#define DOUBLE
#include "kernel/arm/gemv_n.c"