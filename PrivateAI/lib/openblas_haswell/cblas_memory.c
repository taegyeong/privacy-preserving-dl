#include "common.h"
#include <stdio.h>
#include <stdlib.h>

#define BLAS_BUFF_SIZE 8 << 20

#define BLAS_MEM_ENCLAVE
#define BLAS_MEM_REUSE

#ifdef BLAS_MEM_REUSE
void* blas_ptr = -1;

void* blas_memory_alloc(int procpos) {
	if (blas_ptr == -1) {
#ifdef BLAS_MEM_ENCLAVE
		blas_ptr = malloc(BLAS_BUFF_SIZE);
#else
		ocall_malloc(BLAS_BUFF_SIZE, (long long *)&blas_ptr);
#endif
	}
	return blas_ptr;
}

void blas_memory_free(void *free_area) {
}
#else
void* blas_memory_alloc(int procpos) {
	return malloc(BLAS_BUFF_SIZE);
}
void blas_memory_free(void *free_area) {
	free(free_area);
}
#endif