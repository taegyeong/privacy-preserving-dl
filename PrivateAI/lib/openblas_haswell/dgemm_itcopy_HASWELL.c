#define DOUBLE
#define ASMNAME dgemm_itcopy_HASWELL
#define ASMFNAME dgemm_itcopy_HASWELL_
#define NAME dgemm_itcopy_HASWELL_
#define CNAME dgemm_itcopy_HASWELL
#define CHAR_NAME "dgemm_itcopy_HASWELL_"
#define CHAR_CNAME "dgemm_itcopy_HASWELL"
#define DOUBLE
#include "kernel/generic/gemm_tcopy_4.c"