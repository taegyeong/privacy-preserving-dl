#define DOUBLE
#define ASMNAME dgemm_incopy_HASWELL
#define ASMFNAME dgemm_incopy_HASWELL_
#define NAME dgemm_incopy_HASWELL_
#define CNAME dgemm_incopy_HASWELL
#define CHAR_NAME "dgemm_incopy_HASWELL_"
#define CHAR_CNAME "dgemm_incopy_HASWELL"
#define DOUBLE
#include "kernel/generic/gemm_ncopy_4.c"