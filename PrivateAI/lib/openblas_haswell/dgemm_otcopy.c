#define DOUBLE
#define ASMNAME _dgemm_otcopy
#define ASMFNAME _dgemm_otcopy_
#define NAME dgemm_otcopy_
#define CNAME dgemm_otcopy
#define CHAR_NAME "dgemm_otcopy_"
#define CHAR_CNAME "dgemm_otcopy"
#define DOUBLE
#include "kernel/generic/gemm_tcopy_2.c"