#define CBLAS
#define ASMNAME _cblas_sgemv
#define ASMFNAME _cblas_sgemv_
#define NAME cblas_sgemv_
#define CNAME cblas_sgemv
#define CHAR_NAME "cblas_sgemv_"
#define CHAR_CNAME "cblas_sgemv"


#include <stdio.h>
#include "common.h"
#include "l1param.h"
#ifdef FUNCTION_PROFILE
#include "functable.h"
#endif

#ifdef XDOUBLE
#define ERROR_NAME "QGEMV "
#elif defined(DOUBLE)
#define ERROR_NAME "DGEMV "
#else
#define ERROR_NAME "SGEMV "
#endif

#ifdef SMP
static int(*gemv_thread[])(BLASLONG, BLASLONG, FLOAT, FLOAT *, BLASLONG, FLOAT *, BLASLONG, FLOAT *, BLASLONG, FLOAT *, int) = {
#ifdef XDOUBLE
	qgemv_thread_n, qgemv_thread_t,
#elif defined DOUBLE
	dgemv_thread_n, dgemv_thread_t,
#else
	sgemv_thread_n, sgemv_thread_t,
#endif
};
#endif

void cblas_sgemv(enum CBLAS_ORDER order,
	enum CBLAS_TRANSPOSE TransA,
	blasint m, blasint n,
	FLOAT alpha,
	FLOAT  *a, blasint lda,
	FLOAT  *x, blasint incx,
	FLOAT beta,
	FLOAT  *y, blasint incy) {

	FLOAT *buffer;
	blasint lenx, leny;
	int trans, buffer_size;
	blasint info, t;
#ifdef SMP
	int nthreads;
#endif

	int(*gemv[])(BLASLONG, BLASLONG, BLASLONG, FLOAT, FLOAT *, BLASLONG, FLOAT *, BLASLONG, FLOAT *, BLASLONG, FLOAT *) = {
		GEMV_N, GEMV_T,
	};

	PRINT_DEBUG_CNAME;

	trans = -1;
	info = 0;

	if (order == CblasColMajor) {
		if (TransA == CblasNoTrans)     trans = 0;
		if (TransA == CblasTrans)       trans = 1;
		if (TransA == CblasConjNoTrans) trans = 0;
		if (TransA == CblasConjTrans)   trans = 1;

		info = -1;

		if (incy == 0)	  info = 11;
		if (incx == 0)	  info = 8;
		if (lda < MAX(1, m))  info = 6;
		if (n < 0)		  info = 3;
		if (m < 0)		  info = 2;
		if (trans < 0)        info = 1;

	}

	if (order == CblasRowMajor) {
		if (TransA == CblasNoTrans)     trans = 1;
		if (TransA == CblasTrans)       trans = 0;
		if (TransA == CblasConjNoTrans) trans = 1;
		if (TransA == CblasConjTrans)   trans = 0;

		info = -1;

		t = n;
		n = m;
		m = t;

		if (incy == 0)	  info = 11;
		if (incx == 0)	  info = 8;
		if (lda < MAX(1, m))  info = 6;
		if (n < 0)		  info = 3;
		if (m < 0)		  info = 2;
		if (trans < 0)        info = 1;

	}

	if (info >= 0) {
#ifndef ENCLAVE
		BLASFUNC(xerbla)(ERROR_NAME, &info, sizeof(ERROR_NAME));
#endif
		return;
	}

	if ((m == 0) || (n == 0)) return;

	lenx = n;
	leny = m;
	if (trans) lenx = m;
	if (trans) leny = n;

	if (beta != ONE) SCAL_K(leny, 0, 0, beta, y, abs(incy), NULL, 0, NULL, 0);

	if (alpha == ZERO) return;

	IDEBUG_START;

	FUNCTION_PROFILE_START();

	if (incx < 0) x -= (lenx - 1) * incx;
	if (incy < 0) y -= (leny - 1) * incy;

	buffer_size = m + n + 128 / sizeof(FLOAT);
#ifdef WINDOWS_ABI
	buffer_size += 160 / sizeof(FLOAT);
#endif
	// for alignment
	buffer_size = (buffer_size + 3) & ~3;
	//	STACK_ALLOC(buffer_size, FLOAT, buffer);
	buffer = (FLOAT*)malloc(buffer_size * sizeof(FLOAT));
	ocall_print("malloc");
	ocall_print_int(buffer_size * 4);
	ocall_print("\n");
#ifdef SMP

	if (1L * m * n < 2304L * GEMM_MULTITHREAD_THRESHOLD)
		nthreads = 1;
	else
		nthreads = num_cpu_avail(2);

	if (nthreads == 1) {
#endif

		(gemv[(int)trans])(m, n, 0, alpha, a, lda, x, incx, y, incy, buffer);

#ifdef SMP
	}
	else {

		(gemv_thread[(int)trans])(m, n, alpha, a, lda, x, incx, y, incy, buffer, nthreads);

	}
#endif

	//	STACK_FREE(buffer);
	free(buffer);
	FUNCTION_PROFILE_END(1, m * n + m + n, 2 * m * n);

	IDEBUG_END;

	return;

}
