#include "common.h"

int scopy_k(BLASLONG n, FLOAT *x, BLASLONG inc_x, FLOAT *y, BLASLONG inc_y)
{
	BLASLONG i = 0;
	BLASLONG ix = 0, iy = 0;

	if (n < 0)  return(0);

	while (i < n)
	{

		y[iy] = x[ix];
		ix += inc_x;
		iy += inc_y;
		i++;

	}
	return(0);

}


