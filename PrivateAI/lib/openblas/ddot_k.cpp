#define DOUBLE

#include "common.h"

FLOAT ddot_k(BLASLONG n, FLOAT *x, BLASLONG inc_x, FLOAT *y, BLASLONG inc_y)
{
	BLASLONG i = 0;
	BLASLONG ix = 0, iy = 0;
	double dot = 0.0;

	if (n < 0)  return(dot);

	while (i < n)
	{

		dot += y[iy] * x[ix];
		ix += inc_x;
		iy += inc_y;
		i++;

	}
	return(dot);

}


