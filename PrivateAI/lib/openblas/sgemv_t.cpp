#define TRANS
#define ASMNAME _sgemv_t
#define ASMFNAME _sgemv_t_
#define NAME sgemv_t_
#define CNAME sgemv_t
#define CHAR_NAME "sgemv_t_"
#define CHAR_CNAME "sgemv_t"

#include "common.h"

int CNAME(BLASLONG m, BLASLONG n, BLASLONG dummy1, FLOAT alpha, FLOAT *a, BLASLONG lda, FLOAT *x, BLASLONG inc_x, FLOAT *y, BLASLONG inc_y, FLOAT *buffer)
{
	BLASLONG i;
	BLASLONG ix, iy;
	BLASLONG j;
	FLOAT *a_ptr;
	FLOAT temp;

	iy = 0;
	a_ptr = a;

	for (j = 0; j<n; j++)
	{
		temp = 0.0;
		ix = 0;
		for (i = 0; i<m; i++)
		{
			temp += a_ptr[i] * x[ix];
			ix += inc_x;
		}
		y[iy] += alpha * temp;
		iy += inc_y;
		a_ptr += lda;
	}
	return(0);

}


