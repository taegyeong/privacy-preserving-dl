#define DOUBLE

#include "common.h"
#include <math.h>

#define ABS fabs

FLOAT dasum_k(BLASLONG n, FLOAT *x, BLASLONG inc_x)
{
	BLASLONG i = 0;
	FLOAT sumf = 0.0;
	if (n <= 0 || inc_x <= 0) return(sumf);

	n *= inc_x;
	while (i < n)
	{
		sumf += ABS(x[i]);
		i += inc_x;
	}
	return(sumf);
}


