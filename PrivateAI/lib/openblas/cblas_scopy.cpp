#include <stdio.h>
#include "common.h"

void cblas_scopy(blasint n, FLOAT *x, blasint incx, FLOAT *y, blasint incy) {

	PRINT_DEBUG_CNAME;

	if (n <= 0) return;

	IDEBUG_START;

	FUNCTION_PROFILE_START();

	if (incx < 0) x -= (n - 1) * incx * COMPSIZE;
	if (incy < 0) y -= (n - 1) * incy * COMPSIZE;

	COPY_K(n, x, incx, y, incy);

	FUNCTION_PROFILE_END(COMPSIZE, COMPSIZE * n, 0);

	IDEBUG_END;

	return;

}
