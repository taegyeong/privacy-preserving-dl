#define DOUBLE

#include <stdio.h>
#include "common.h"

FLOAT cblas_dasum(blasint n, FLOAT *x, blasint incx) {

	FLOAT ret;

	PRINT_DEBUG_CNAME;

	if (n <= 0) return 0;

	IDEBUG_START;

	FUNCTION_PROFILE_START();

	ret = ASUM_K(n, x, incx);

	FUNCTION_PROFILE_END(COMPSIZE, n, n);

	IDEBUG_END;

	return ret;
}
