#define DOUBLE

#include "common.h"

int dgemv_n(BLASLONG m, BLASLONG n, BLASLONG dummy1, FLOAT alpha, FLOAT *a, BLASLONG lda, FLOAT *x, BLASLONG inc_x, FLOAT *y, BLASLONG inc_y, FLOAT *buffer)
{
	BLASLONG i;
	BLASLONG ix, iy;
	BLASLONG j;
	FLOAT *a_ptr;
	FLOAT temp;

	ix = 0;
	a_ptr = a;

	for (j = 0; j<n; j++)
	{
		temp = alpha * x[ix];
		iy = 0;
		for (i = 0; i<m; i++)
		{
			y[iy] += temp * a_ptr[i];
			iy += inc_y;
		}
		a_ptr += lda;
		ix += inc_x;
	}
	return(0);
}


