#include <stdio.h>
#include "common.h"

FLOAT cblas_sdot(blasint n, FLOAT *x, blasint incx, FLOAT *y, blasint incy) {

	FLOAT ret;

	PRINT_DEBUG_CNAME;

	if (n <= 0) return 0.;

	IDEBUG_START;

	FUNCTION_PROFILE_START();

	if (incx < 0) x -= (n - 1) * incx;
	if (incy < 0) y -= (n - 1) * incy;

	ret = DOTU_K(n, x, incx, y, incy);

	FUNCTION_PROFILE_END(1, 2 * n, 2 * n);

	IDEBUG_END;

	return ret;

}

