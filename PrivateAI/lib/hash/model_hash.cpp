#include <sgx_thread.h>
#include <map>
//#include <vector>

#include "PrivateAI_t.h"
#include "hash/model_hash.h"
#include "hash/Crc32.h"
#include "hash/md5.h"
#include "hash/sha256.h"
#include "hash/xxhash32.h"
#include "hash/xxhash64.h"

//#define HASH_LOG

void print_hash_int(hash_id id, int precomp, int newcomp) {
	ocall_print("[");
	ocall_print_int(id);
	ocall_print("] ");
	ocall_print_int(precomp);
	ocall_print("\n  ");
	ocall_print_int(newcomp);
	ocall_print("\n");
}
void print_hash_string(hash_id id, string precomp, string newcomp) {
	ocall_print("[");
	ocall_print_int(id);
	ocall_print("] ");
	ocall_print(precomp.c_str());
	ocall_print("\n  ");
	ocall_print(newcomp.c_str());
	ocall_print("\n");
}
void HashABCT<uint32_t>::print_hash(hash_id id) {
	print_hash_int(id, blob_hashs[id].hash_value,
		get_hash_value(blob_hashs[id].ptr, blob_hashs[id].size));
}
void HashABCT<uint64_t>::print_hash(hash_id id) {
	print_hash_int(id, blob_hashs[id].hash_value,
		get_hash_value(blob_hashs[id].ptr, blob_hashs[id].size));
}
void HashABCT<string>::print_hash(hash_id id) {
	print_hash_string(id, blob_hashs[id].hash_value,
		get_hash_value(blob_hashs[id].ptr, blob_hashs[id].size));
}

class HashNone : public HashABCT<uint32_t> {
public:
	uint32_t get_hash_value(void* ptr, size_t size) { return 0;	}
};
class HashCRC32 : public HashABCT<uint32_t> {
	uint32_t get_hash_value(void* ptr, size_t size) {
		return crc32_16bytes(ptr, size);
	}
};
class HashMD5 : public HashABCT<string> {
	string get_hash_value(void* ptr, size_t size) {
		return MD5()(ptr, size);
	}
};
class HashSHA256 : public HashABCT<string> {
	string get_hash_value(void* ptr, size_t size) {
		return SHA256()(ptr, size);
	}
};
class HashXX32 : public HashABCT<uint32_t> {
	uint32_t get_hash_value(void* ptr, size_t size) {
		return XXHash32::hash(ptr, size, 0);
	}
};
class HashXX64 : public HashABCT<uint64_t> {
	uint64_t get_hash_value(void* ptr, size_t size) {
		return XXHash64::hash(ptr, size, 0);
	}
};

hash_id last_id = -1;
HashABC* model_hash;
hash_func_t hash_function;

void init_hash_function(hash_func_t hash_function_) {
	switch (hash_function_) {
	case HASH_FUNC_NONE:
		model_hash = new HashNone();
		break;
	case HASH_FUNC_CRC32:
		model_hash = new HashCRC32();
		break;
	case HASH_FUNC_MD5:
		model_hash = new HashMD5();
		break;
	case HASH_FUNC_SHA256:
		model_hash = new HashSHA256();
		break;
	case HASH_FUNC_XXHASH32:
		model_hash = new HashXX32();
		break;
	case HASH_FUNC_XXHASH64:
		model_hash = new HashXX64();
		break;
	default:
		ocall_print("Wrong Hash Function!\n");
		break;
	}
	hash_function = hash_function_;
}

string hash_function_name() {
	switch (hash_function) {
	case HASH_FUNC_NONE:
		return "NONE";
	case HASH_FUNC_CRC32:
		return "CRC32";
	case HASH_FUNC_MD5:
		return "MD5";
	case HASH_FUNC_SHA256:
		return "SHA256";
	case HASH_FUNC_XXHASH32:
		return "xxHash32";
	case HASH_FUNC_XXHASH64:
		return "xxHash64";
	}
	return "Unknown";
}

hash_id set_blob_hash(void* ptr, int blob_count) {
	return model_hash->create_blob_hash(ptr, blob_count * sizeof(float));
}

void corruption_detected() {
	ocall_sys_exit("weight data corruption detected");
}

void forward_hash_check(hash_id id) {
#ifdef HASH_LOG
	ocall_hashing_start();
#endif
	last_id = id;
	if (!model_hash->hash_check(id)) {
		model_hash->print_hash(id);
		corruption_detected();
	}
#ifdef HASH_LOG
	ocall_hashing_finish();
#endif
}
void forward_hash_check_copy(hash_id id, void* ptr) {
#ifdef HASH_LOG
	ocall_hashing_start();
#endif
	rd_hash_reset();
	if (!model_hash->hash_check_copy(id, ptr)) {
		corruption_detected();
	}
#ifdef HASH_LOG
	ocall_hashing_finish();
#endif
}

void rd_hash_reset() {
	last_id = -1;
}

size_t rd_hash_check() {
	int last_id_ = last_id;
	if (last_id_ < 0)
		return 0;
	if (!model_hash->hash_check(last_id_)) {
		corruption_detected();
	}
	return model_hash->get_size(last_id_);
}

void ecall_test() {
	ocall_print("ecall_test\n");
}