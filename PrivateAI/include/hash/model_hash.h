#ifndef RD_HASH_H
#define RD_HASH_H

#include <vector>

using std::vector;
using std::string;

typedef int hash_id;
typedef enum hash_func_t {
	HASH_FUNC_NONE,
	HASH_FUNC_CRC32,
	HASH_FUNC_MD5,
	HASH_FUNC_SHA256,
	HASH_FUNC_XXHASH32,
	HASH_FUNC_XXHASH64
};

class HashABC {
public:
	virtual hash_id create_blob_hash(void* ptr, size_t size) = 0;
	virtual bool hash_check(hash_id id) = 0;
	virtual bool hash_check_copy(hash_id id, void* ptr) = 0;
	virtual void print_hash(hash_id id) = 0;
	virtual size_t get_size(hash_id id) = 0;
};

template<typename hash_value_t>
class HashABCT : public HashABC {
private:
	struct blob_hash {
		hash_value_t hash_value;
		void* ptr;
		size_t size;
	};
	vector<blob_hash> blob_hashs;
	virtual hash_value_t get_hash_value(void* ptr, size_t size) = 0;
public:
	hash_id create_blob_hash(void* ptr, size_t size) {
		blob_hashs.push_back({ get_hash_value(ptr, size), ptr, size });
		return blob_hashs.size() - 1;
	}
	bool hash_check(hash_id id) {
		return blob_hashs[id].hash_value
			== get_hash_value(blob_hashs[id].ptr, blob_hashs[id].size);
	}
	bool hash_check_copy(hash_id id, void* ptr) {
		memcpy(ptr, blob_hashs[id].ptr, blob_hashs[id].size);
		return blob_hashs[id].hash_value
			== get_hash_value(ptr, blob_hashs[id].size);
	}
	size_t get_size(hash_id id) {
		return blob_hashs[id].size;
	}
	void print_hash(hash_id id);
};

void init_hash_function(hash_func_t hash_function);
string hash_function_name();
void rd_hash_reset();
hash_id set_blob_hash(void* ptr, int size);
void forward_hash_check(hash_id id);
void forward_hash_check_copy(hash_id id, void* ptr);

#endif