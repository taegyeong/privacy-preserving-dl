#ifndef MY_STREAM_HPP
#define MY_STREAM_HPP

#include "PrivateAI_t.h"

#include <string>
using std::string;

const int buffer_length = 256;

class myStream;
namespace std
{
	typedef myStream istream;
	typedef myStream ostream;
	typedef myStream ostringstream;
	inline myStream& endl(myStream& _o)
	{
		return _o;
	}
}

// TODO: output with fillchar and fillwidth
class myStream
{
	string buffer;
	char tmpBuf[buffer_length];
	bool ignore;
	char fillchar;
	int fillwidth;
public:
	myStream() : ignore(true), buffer("") {}
	myStream(bool _i) : ignore(_i), buffer("") {}

	myStream& operator << (int x)
	{
		if (!ignore)
		{
			if (x < 0)
			{
				buffer.push_back('-');
				x = -x;
			}
			if (x == 0)
				buffer.push_back('0');
			else
			{
				int i = 0;
				for (; x; x /= 10)
					tmpBuf[i++] = (char)('0' + x % 10);
				for (int j = i - 1; j >= 0; --j)
					buffer.push_back(tmpBuf[j]);
			}
		}
		return *this;
	}
	myStream& operator << (unsigned int x)
	{
		if (!ignore)
		{
			if (x == 0)
				buffer.push_back('0');
			else
			{
				int i = 0;
				for (; x; x /= 10)
					tmpBuf[i++] = (char)('0' + x % 10);
				for (int j = i - 1; j >= 0; --j)
					buffer.push_back(tmpBuf[j]);
			}
		}
		return *this;
	}
	myStream& operator << (unsigned __int64 x)
	{
		if (!ignore)
		{
			if (x == 0)
				buffer.push_back('0');
			else
			{
				int i = 0;
				for (; x; x /= 10)
					tmpBuf[i++] = (char)('0' + x % 10);
				for (int j = i - 1; j >= 0; --j)
					buffer.push_back(tmpBuf[j]);
			}
		}
		return *this;
	}
	myStream& operator << (string x)
	{
		if (!ignore) buffer += x;
		return *this;
	}
	myStream& operator << (double x)
	{
		if (!ignore)
			*this << (int)(x * 100) << "/100";
		return *this;
	}
	myStream& operator << (myStream& (*op)(myStream&))
	{
		if (!ignore)
		{
			buffer.push_back('\n');
			ocall_print(buffer.c_str());
			buffer = "";
		}
		return *this;
	}
	myStream& write(const char* s, int n)
	{
		*this << string(s, n);
		return *this;
	}
	myStream& read(char *s, int n)
	{
		return *this;
	}
	bool eof() const { return false; }
	bool good() const { return true; }
	bool fail() const { return false; }
	int gcount() const { return 0; }
	string str() { return buffer; }
	void setfill(char _c) { fillchar = _c; }
	void setw(int _w) { fillwidth = _w; }
};

#endif