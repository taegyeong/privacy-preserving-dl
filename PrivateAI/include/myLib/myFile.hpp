#ifndef MY_FILE_HPP
#define MY_FILE_HPP

#include "PrivateAI_t.h"

#include <unordered_map>
using std::unordered_map;

struct iovec {
	void* iov_base; /* Starting address */
	size_t iov_len; /* Length in bytes */
};

const int O_RDONLY = 1 << 0;
const int O_BINARY = 1 << 1;

struct FILE
{
};

extern FILE* stdin;
extern FILE* stdout;
extern FILE* stderr;

struct file_t
{
	long long ptr, size, cur;
	file_t() {}
	file_t(long long _ptr, long long _size)
	{
		cur = ptr = _ptr; size = _size;
	}
};

extern unordered_map<int, file_t> file_pool;

inline int open(const char* filename, const int mode)
{
	int fd;
	long long ptr, size;
	ocall_open(&fd, &ptr, &size, filename, mode);
	file_pool[fd] = file_t(ptr, size);
	return fd;
}

inline void close(int fd)
{
	ocall_close(fd);
	file_pool.erase(fd);
}

inline int myRead(int fd, void* buffer, int size)
{
	file_t* file = &(file_pool.find(fd)->second);
	int _size = file->size - (file->cur - file->ptr);
	_size = (size < _size) ? size : _size;
	unsigned char* dst = (unsigned char*)buffer;
	unsigned char* src = (unsigned char*)file->cur;
	for (int i = 0; i < _size; ++i, ++dst, ++src)
		*dst = *src;
//	memcpy(buffer, (void*)file->cur, sizeof(char) * _size);
	file->cur += _size;
	return _size;
}

#endif