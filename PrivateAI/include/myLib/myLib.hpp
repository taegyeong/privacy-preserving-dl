#ifndef MYLOG_H
#define MYLOG_H

//----------------------------------------------------------------------------------------------//

namespace google {
	namespace protobuf {
		namespace internal {
			inline int Barrier_AtomicIncrement(int volatile *ptr, int increment)
			{
				return 0;
			}
			inline int NoBarrier_CompareAndSwap(int volatile *ptr, int old_value, int new_value)
			{
				return 0;
			}
			namespace win32 {
				inline int close(int fd)
				{
					return 0;
				}
				inline int read(int fd, void* buffer, unsigned int size)
				{
					return 0;
				}
				inline int write(int fd, const void* buffer, unsigned int size)
				{
					return 0;
				}
			}
		}
	}
}


//----------------------------------------------------------------------------------------------//

inline double my_uint64_to_double(unsigned __int64 x)
{
	return 0;
}

inline double my_int64_to_double(signed __int64 x)
{
	return 0;
}

inline unsigned __int64 my_double_to_uint64(double x)
{
	return 0;
}

inline signed __int64 my_double_to_int64(double x)
{
	return 0;
}

inline __int64 _strtoi64(const char *nptr, char **endptr, int base)
{
	return 0;
}

inline __int64 _strtoui64(const char *nptr, char **endptr, int base)
{
	return 0;
}

//----------------------------------------------------------------------------------------------//

#include "myLib/myFile.hpp"

inline int printf(const char *format, ...)
{
	return 0;
}

inline int fprintf(FILE* fp, const char *format, ...)
{
	return 0;
}

inline int sprintf(char *des, const char *format, ...)
{
	return 0;
}

inline int _snprintf(char *des, size_t n, const char *format, ...)
{
	return 0;
}

//----------------------------------------------------------------------------------------------//

inline char* strcpy(char *des, const char *src)
{
	while (*src != '\0')
	{
		*des = *src;
		++des;
		++src;
	}
	*des = '\0';
	return des;
}

inline char *strdup(char *s)
{
	int len = 0;
	while (s[len] != '\0')
		++len;
	char *des = (char*)malloc(sizeof(char) * (len + 1));
	strcpy(des, s);
	return des;
}

inline char *strdup(const char *s)
{
	return strdup((char*)s);
}

//----------------------------------------------------------------------------------------------//

inline unsigned short my_byteswap_ushort(unsigned short x)
{
	return ((x & 0x0F) << 8) | ((x & 0xF0) >> 8);
}

inline unsigned long my_byteswap_ulong(unsigned long x)
{
	return (my_byteswap_ushort(x & 0x00FF) << 16) | (my_byteswap_ulong((x & 0xFF00) >> 16));
}

inline unsigned __int64 my_byteswap_uint64(unsigned __int64 x)
{
	return (((__int64)my_byteswap_ushort(x & 0x0000FFFF)) << 32) | (my_byteswap_ulong((x & 0xFFFF0000) >> 32));
}

//----------------------------------------------------------------------------------------------//

#endif