#ifndef MY_GLOG_HPP
#define MY_GLOG_HPP

#include "myLib/myStream.hpp"

#include <boost/shared_ptr.hpp>
using boost::shared_ptr;

const int NUM_LEVEL = 5;

enum LOG_t {
	INFO	= 0, 
	WARNING = 1, 
	ERROR	= 2, 
	FATAL	= 3, 
	DISCARD = 4 
};

extern myStream myLog[NUM_LEVEL];
extern myStream myDLog[NUM_LEVEL];
extern myStream myCheck;
extern myStream myDCheck;

inline myStream& LOG(LOG_t _i)
{
	return myLog[_i];
}

inline myStream& LOG_IF(LOG_t _i, bool _b)
{
	if (_b) return myLog[_i];
	else return myLog[DISCARD];
}

inline myStream& LOG_EVERY_N(LOG_t _i, int _N)
{
	static int cnt = 0;
	if (++cnt == _N)
	{
		cnt = 0;
		return myLog[_i];
	}
	else return myLog[DISCARD];
}

inline myStream& DLOG(LOG_t _i)
{
	return myDLog[_i];
}

inline myStream& CHECK(bool _b)
{
	if (_b)
		return myCheck;
	else
		return myLog[DISCARD];
}

inline myStream& CHECK_EQ(int _a, int _b)
{
	return CHECK(_a == _b);
}

inline myStream& CHECK_NE(int _a, int _b)
{
	return CHECK(_a != _b);
}

inline myStream& CHECK_GT(int _a, int _b)
{
	return CHECK(_a > _b);
}

inline myStream& CHECK_GE(int _a, int _b)
{
	return CHECK(_a >= _b);
}

inline myStream& CHECK_LT(int _a, int _b)
{
	return CHECK(_a < _b);
}

inline myStream& CHECK_LE(int _a, int _b)
{
	return CHECK(_a <= _b);
}

inline myStream& DCHECK(bool _b)
{
	if (_b)
		return myDCheck;
	else
		return myLog[DISCARD];
}

inline myStream& DCHECK_EQ(int _a, int _b)
{
	return DCHECK(_a == _b);
}

inline myStream& DCHECK_NE(int _a, int _b)
{
	return DCHECK(_a != _b);
}

inline myStream& DCHECK_GT(int _a, int _b)
{
	return DCHECK(_a > _b);
}

inline myStream& DCHECK_GE(int _a, int _b)
{
	return DCHECK(_a >= _b);
}

inline myStream& DCHECK_LT(int _a, int _b)
{
	return DCHECK(_a < _b);
}

inline myStream& DCHECK_LE(int _a, int _b)
{
	return DCHECK(_a <= _b);
}

inline myStream& CHECK_EQ(string _a, string _b)
{
	return CHECK(_a == _b);
}

inline myStream& CHECK_NE(void* _a, void* _b)
{
	return CHECK(_a != _b);
}

template <typename T>
inline T* CHECK_NOTNULL(T* _p)
{
	if (_p == NULL) 
		myCheck << "shared_ptr is NULL";
	return _p;
}

template <typename T>
inline myStream& CHECK(shared_ptr<T> _p)
{
	return CHECK(_p == NULL);
}

#endif
