#ifndef MY_MEMORY_HPP
#define MY_MEMORY_HPP

typedef enum memory_flag {
	OCALL_MEM_ONLY = -1,
	ENCLAVE_ALL = 0,
	ENCLAVE_EXCEPT_BLAS = 1
};

int get_memory_flag();
void set_memory_flag(int flag);

void* myMalloc(size_t size);
void myFree(void* ptr);

void myMemorySize(void *ptr);


#endif