#ifndef CAFFE_UTIL_FORMAT_H_
#define CAFFE_UTIL_FORMAT_H_

#include <iomanip>  // NOLINT(readability/streams)
#include <sstream>  // NOLINT(readability/streams)
#include <string>

#include "myLib/myStream.hpp"

namespace caffe {

#ifdef ENCLAVE
inline std::string format_int(int n, int numberOfLeadingZeros = 0 ) {
  myStream s;
  s << n;
  return s.str();
}
#endif

}

#endif   // CAFFE_UTIL_FORMAT_H_
