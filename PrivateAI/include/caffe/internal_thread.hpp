#ifndef CAFFE_INTERNAL_THREAD_HPP_
#define CAFFE_INTERNAL_THREAD_HPP_

#include "caffe/common.hpp"

#include "sgx_thread.h"

namespace caffe {

/**
 * Virtual class encapsulate boost::thread for use in base class
 * The child class will acquire the ability to run a single thread,
 * by reimplementing the virtual function InternalThreadEntry.
 */
class InternalThread {

private:

	static const int maxTotalThread = 6;

	static int totalThread;
	static sgx_thread_mutex_t mutex;

public:

  static vector<InternalThread*> _thread;

  InternalThread();
  virtual ~InternalThread();

  /**
   * Caffe's thread local state will be initialized using the current
   * thread values, e.g. device id, solver index etc. The random seed
   * is initialized using caffe_rng_rand.
   */
  void StartInternalThread();

  /** Will not return until the internal thread has exited. */
  void StopInternalThread();

  bool is_started() const;

  void entry(int device, Caffe::Brew mode, int rand_seed,
	  int solver_count, int solver_rank, bool multiprocess);

 protected:
  /* Implement this method in your subclass
      with the code you want your thread to run. */
  virtual void InternalThreadEntry() {}

  /* Should be tested when running loops to exit when requested. */
  bool must_stop();

  int index;
  size_t tid;
  bool should_stop;
};

}  // namespace caffe

#endif  // CAFFE_INTERNAL_THREAD_HPP_
