#ifndef PREDEF_H
#define PREDEF_H

/* MM Division */
#define COL_BUFF_LIMIT 8 << 20
//#define FORCE_FILTER_DIV 4
#define FC_WEIGHT_LIMIT 8 << 20

/* Copy vs Read */
#define COPY_WEIGHT_ALL


/* Hashing */
#ifndef COPY_WEIGHT_ALL
#define PARALLEL_HASH
#endif

/* Interface */
//#define SHOW_PROGRESS

#ifdef COPY_WEIGHT_ALL
#ifndef COPY_WEIGHT
#define COPY_WEIGHT
#endif
#endif

#endif
