#include "PrivateAI_t.h"
#include "sgx_trts.h"

#include <string>
#include "myLib/myMemory.hpp"
#include "predef.h"

#include "caffe/net.hpp"
#include "caffe/proto/caffe.pb.h"
#include "caffe/util/io.hpp"

using namespace std;
using namespace caffe;

typedef std::pair<int, float> Prediction;

int trial_;

class Classifier {
public:
	Classifier(const string& model_file, float** weight_ptr);
	std::vector<Prediction> Classify(const string image_file, int N = 5);

private:
	std::vector<float> Predict(const string image_file);

public:
	Net<float>* net_;
	std::vector<int> labels_;
};

/* Load model and weights for inference */
Classifier::Classifier(const string& model_file, float** weight_ptr)
{
#ifdef CPU_ONLY
	Caffe::set_mode(Caffe::CPU);
#else
	Caffe::set_mode(Caffe::GPU);
#endif

	/* Load the network. */
	net_ = new Net<float>(model_file, TEST);
	net_->CopyTrainedLayersFromPtr(weight_ptr);
	
	Blob<float>* output_layer = net_->output_blobs()[0];
	int num = output_layer->channels();
	for (int i = 0; i < num; ++i)
		labels_.push_back(i);
}

static bool PairCompare(const std::pair<float, int>& lhs, const std::pair<float, int>& rhs)
{
	return lhs.first > rhs.first;
}

/* Return the indices of the top N values of vector v. */
static std::vector<int> Argmax(const std::vector<float>& v, int N)
{
	std::vector<std::pair<float, int> > pairs;
	for (size_t i = 0; i < v.size(); ++i)
		pairs.push_back(std::make_pair(v[i], i));
	std::partial_sort(pairs.begin(), pairs.begin() + N, pairs.end(), PairCompare);

	std::vector<int> result;
	for (int i = 0; i < N; ++i)
		result.push_back(pairs[i].second);
	return result;
}

/* Return the top N predictions. */
std::vector<Prediction> Classifier::Classify(const string image_file, int N) {
	std::vector<float> output = Predict(image_file);

	N = std::min<int>(labels_.size(), N);
	std::vector<int> maxN = Argmax(output, N);
	std::vector<Prediction> predictions;
	for (int i = 0; i < N; ++i) {
		int idx = maxN[i];
		predictions.push_back(std::make_pair(labels_[idx], output[idx]));
	}
	return predictions;
}

/* Read image from file and predict */
std::vector<float> Classifier::Predict(const string image_file)
{
	//ocall_start();
	Blob<float>* input_layer = net_->input_blobs()[0];
	int num_channels_ = input_layer->channels();
	int height_ = input_layer->height();
	int width_ = input_layer->width();

	Datum datum;
	if (!ReadImageToDatum(image_file, 0, height_, width_, num_channels_ == 3, &datum))
		LOG(ERROR) << "Error during file reading" << endl;
	Blob<float>* blob = new Blob<float>(1, datum.channels(), datum.height(), datum.width());

	BlobProto blob_proto;
	blob_proto.set_num(1);
	blob_proto.set_channels(datum.channels());
	blob_proto.set_height(datum.height());
	blob_proto.set_width(datum.width());
	int size_in_datum = std::max<int>(datum.data().size(),
		datum.float_data_size());

	for (int ii = 0; ii < size_in_datum; ++ii) {
		blob_proto.add_data(0.);
	}
	const string& data = datum.data();
	if (data.size() != 0) {
		for (int ii = 0; ii < size_in_datum; ++ii) {
			blob_proto.set_data(ii, blob_proto.data(ii) + (uint8_t)data[ii]);
		}
	}

	// Set data into blob
	blob->FromProto(blob_proto);

	// Fill the vector
	vector<Blob<float>*> bottom;
	bottom.push_back(blob);
	float type = 0.0;

	ocall_start();
	const vector<Blob<float>*>& result = net_->Forward(bottom, &type);
	ocall_finish("Inference", trial_);
	for (Blob<float>* bb : bottom) {
		delete bb;
	}

	/* Copy the output layer to a std::vector */
	Blob<float>* output_layer = net_->output_blobs()[0];
	const float* begin = output_layer->cpu_data();
	const float* end = begin + output_layer->channels();
	return std::vector<float>(begin, end);
}

Classifier *classifier = NULL;
int verbose = 2;

void set_configs(int verbose_) {
	verbose = verbose_;
}

void load_model(const char* model_file, long long weight_ptr, int hash_function)
{
	if (verbose > 1)
		ocall_print("\n[ECALL] Load Model\n");

	set_memory_flag(OCALL_MEM_ONLY);

	init_hash_function((hash_func_t)hash_function);

	if (verbose > 0) {
		ocall_print("Hash Function: ");
		ocall_print(hash_function_name().c_str());
		ocall_print("\n");
	}

	if (verbose > 0) {
#ifdef COPY_WEIGHT_ALL
		ocall_print("Weight access: COPY ALL\n");
#elif defined(COPY_WEIGHT)
		ocall_print("Weight access: COPY (conv)\n");
#else
		ocall_print("Weight access: READ OUTSIDE\n");
#endif
#ifdef PARALLEL_HASH
		ocall_print(" - Additionally Hash in parallel\n");
#endif
	}
	classifier = new Classifier((string)model_file, (float**)weight_ptr);
	if (verbose > 0)
		ocall_print("\n");
}

#define LOG_AVG

void inference(const char* image_file, int trial)
{
	if (verbose > 1) {
		ocall_print("[ECALL] Inference (trial: ");
		ocall_print_int(trial);
		ocall_print(")\n");
	}

	// tglee: memory location
	//set_memory_flag(OCALL_MEM_ONLY);
	set_memory_flag(ENCLAVE_ALL);

	std::vector<Prediction> predictions;
#ifdef LOG_AVG
	trial_ = -1;
#endif
#ifdef SHOW_PROGRESS
	if (trial > 10)
		ocall_progress_init(trial);
#endif
	for (int i = 0; i < trial; i++) {
#ifndef LOG_AVG
		trial_ = i;
#endif
		predictions = classifier->Classify(string(image_file));
#ifdef PARALLEL_HASH
		ocall_rdhash_join();
#endif
#ifdef SHOW_PROGRESS
		ocall_progress(i);
#endif
	}
	if (verbose > 1) {
		if (trial > 0) {
			/* Print the top N predictions. */
			for (size_t i = 0; i < predictions.size(); ++i) {
				Prediction p = predictions[i];
				ocall_print_result(p.first, p.second);
			}
		}
		ocall_print("\n");
	}

#ifdef LOG_AVG
	//ocall_mem_log_print(trial);
	//ocall_hash_log_print(trial);
#else
	ocall_time_log_print(1);
	ocall_hash_log_print(1);
#endif
}