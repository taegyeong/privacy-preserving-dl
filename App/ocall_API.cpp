#include "PrivateAI_u.h"
#include "myLib\my_time_log.h"

#include <windows.h>
#include <opencv.hpp>

#include <string>
#include <algorithm>
#include <unordered_map>
#include <vector>
#include <thread>
#include <random>
#include <chrono>
#include <set>

#include "caffe/proto/caffe.pb.h"
#include "caffe/util/io.hpp"

extern sgx_enclave_id_t global_eid;

using std::string;
using std::unordered_map;
using std::vector;
using std::thread;
using std::set;
using time_point = std::chrono::time_point<std::chrono::high_resolution_clock>;

extern void print_prediction(int idx, float output);
extern void print_elapsed_time(const char* msg, int time);

unordered_map<string, int> memory_usages;
vector<int> im2col_logs;
string type_im2col = "im2col";
string type_hash = "hash";
string type_rd_hash = "rd_hash";
unordered_map<string, int> time_log;
set<string> type_exclude_total({ type_hash, type_rd_hash });
vector<string> type_exclude_total_ = vector<string>({type_hash, type_rd_hash });

time_point start_t, finish_t;
time_point hash_start_t, hash_finish_t;
time_point get_us_time() {
	return std::chrono::high_resolution_clock::now();
}
int count_duration(time_point start, time_point finish) {
	return std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
}

bool rdhash = false;
bool rdhash_thread_halt;
int rdhash_count = 0;
int rdhash_time = 0;
int forward_hash_time = 0;
size_t rdhash_total_size;
thread rdhash_thread;
void rd_hash_loop() {
	time_point rd_start_t, rd_finish_t;
	size_t rdhash_size;
	while (!rdhash_thread_halt) {
		rd_start_t = get_us_time();
		rd_hash_check(global_eid, &rdhash_size);
		rd_finish_t = get_us_time();
		if (rdhash_size > 0) {
			rdhash_total_size += rdhash_size;
			rdhash_time += count_duration(rd_start_t, rd_finish_t);
			rdhash_count++;
		}
		//Sleep(10);
	}
}
void ocall_rdhash_start() {
	rdhash_thread_halt = false;
	rdhash = true;
	rdhash_thread = thread(&rd_hash_loop);
}
void ocall_rdhash_stop() {
	rdhash_thread_halt = true;
}
void ocall_hash_log_print(int trial) {
	if (!rdhash && forward_hash_time == 0)
		return;
	printf("  < Hashing (trials: %d) >\n", trial);
	printf("  %s\n", std::string(32, '-').c_str());
	if (forward_hash_time > 0)
		printf("   %-18s%9.3f ms\n", "FW Hash Time", forward_hash_time / 1000. / trial);
	if (rdhash) {
		printf("   %-18s%9.3f ms\n", "RD Hash Time", rdhash_time / 1000. / trial);
		printf("   %-18s%12d\n", "RD Hash Num", rdhash_count / trial);
		printf("   %-18s%9d KB\n", "RD Hash Memory", rdhash_total_size / 1024 / trial);
	}
	printf("  %s\n\n", std::string(32, '-').c_str());
}
void ocall_rdhash_join() {
	if (rdhash_thread.joinable())
		rdhash_thread.join();
}
void ocall_hashing_start() {
	hash_start_t = get_us_time();
}
void ocall_hashing_finish() {
	hash_finish_t = get_us_time();
	//print_elapsed_time(message, finish_t - start_t);
	forward_hash_time +=
		count_duration(hash_start_t, hash_finish_t);
}

void ocall_sys_exit(const char* message) {
	printf("EXIT: %s\n", message);
	system("pause");
	exit(-1);
}
 
void ocall_malloc(size_t size, long long *ptr) {
	//printf("     - ocall_malloc %9.6f\n", (float)size / 1000000.0);
	void* new_ptr = malloc(size);
	*ptr = (long long)new_ptr;
	//printf(" -- ocall_malloc (ptr: %p - %p)\n", new_ptr, (long long)new_ptr + size);
}
void ocall_free(long long ptr) {
	free((void *)ptr);
}
void ocall_mem_log(const char * field, int size) {
	size *= 4;
	//printf("   %-12s%10.6f\n", field, (float)size / 1000000.0);
	string usage_type = field;
	if (memory_usages.count(usage_type) == 0) {
		memory_usages.insert(make_pair(usage_type, 0));
	}
	memory_usages[usage_type] += size;
	if (usage_type.compare("im2col") == 0) {
		im2col_logs.push_back(size);
	}
}
void ocall_mem_log_print(int trial) {
	int memory_total = 0;
	int im2col_total = 0;
	printf("  %10s\n", std::string(28, '-').c_str());
	printf("   %-14s%12s\n", "Type", "Memory size");
	printf("  %s\n", std::string(28, '-').c_str());
	for (auto iter = memory_usages.begin(); iter != memory_usages.end(); iter++) {
		printf("   %-14s%12.6f\n", iter->first.c_str(), (float)iter->second / 1000000.0 / trial);
		memory_total += iter->second;
	}
	printf("  %s\n", std::string(28, '-').c_str());
	if (memory_usages.size() > 1) {
		printf("   %-14s%12.6f\n", "Total", (float)memory_total / 1000000.0 / trial);
		printf("  %s\n", std::string(28, '-').c_str());
	}

	if (im2col_logs.size() == 0)
		return;
	printf("  %10s\n", std::string(22, '-').c_str());
	printf("   %-8s%12s\n", "Layer", "im2col size");
	printf("  %s\n", std::string(22, '-').c_str());
	for (int i = 0; i < im2col_logs.size(); i++) {
		printf("   conv[%2d]%12.6f\n", i, (float)im2col_logs[i] / 1000000.0 / trial);
		im2col_total += im2col_logs[i];
	}
	printf("  %s\n", std::string(22, '-').c_str());
	printf("   %-8s%12.6f\n", "Total", (float)im2col_total / 1000000.0 / trial);
	printf("  %s\n", std::string(22, '-').c_str());
}

//----------------------------------------------------------------------------------------------//

void ocall_print(const char* message) {
	fprintf(stdout, "%s", message);
}

void ocall_print_int(int number) {
	fprintf(stdout, "%d", number);
}

void ocall_print_ptr(long long number) {
	fprintf(stdout, "%p", (void*)number);
}

long mem_s = 0;
void ocall_print_mem(int s) {
	mem_s += s;
	fprintf(stdout, "[MEM SIZE] %d %ld\n", s, mem_s);
}

void ocall_print_float(float number) {
	fprintf(stdout, "%f", number);
}

void ocall_print_result(int number, float output) {
	print_prediction(number, output);
}

//----------------------------------------------------------------------------------------------//
void ocall_start() {
	init_time_log(true);
}

void ocall_finish(const char* message, int num = -1) {
	record_time_log(message, true);
}

int progress_width = 40;
int progress_total = -1;
void ocall_progress_init(int total) {
	progress_total = total;
	std::cout
		<< "[" << string(progress_width, ' ')
		<< "]   0 %\r";
	std::cout.flush();
}
void ocall_progress(int progress) {
	if (progress_total < 0)
		return;
	int pos = progress_width * (progress + 1) / progress_total;
	std::cout
		<< "[" << string(pos, '#') << string(progress_width - pos, ' ')
		<< "] " << std::setfill(' ') << std::setw(3)
		<< (100 * (progress + 1) / progress_total) << " %\r";
	std::cout.flush();
	if(progress + 1 == progress_total)
		std::cout << std::endl;
}

void ocall_newInternalThread(int* index) {}

void ocall_cpuid(int op, int *eax, int *ebx, int *ecx, int *edx) {
//	printf("%d %d %d %d\n", *eax, *ebx, *ecx, *edx);
	int cpuinfo[4];
	__cpuid(cpuinfo, op);
	*eax = cpuinfo[0];
	*ebx = cpuinfo[1];
	*ecx = cpuinfo[2];
	*edx = cpuinfo[3];
//	printf("%d %d %d %d\n", *eax, *ebx, *ecx, *edx);
}

//----------------------------------------------------------------------------------------------//

static bool matchExt(const std::string & fn, std::string en) {
	size_t p = fn.rfind('.');
	std::string ext = p != fn.npos ? fn.substr(p) : fn;
	std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
	std::transform(en.begin(), en.end(), en.begin(), ::tolower);
	if (ext == en)
		return true;
	if (en == "jpg" && ext == "jpeg")
		return true;
	return false;
} 

void ocall_ReadImageToDatum(const char* filename, int label, 
	int height, int width, int is_color, const char* _encoding, long long* datum) {
	caffe::Datum datum_;
	caffe::ReadImageToDatum(filename, label, height, width, is_color, &datum_);
	*datum = (long long)&datum_;
}

cv::Mat ReadImageToCVMat(const string& filename, const int height, const int width, const bool is_color) {
	cv::Mat cv_img;
	int cv_read_flag = (is_color ? CV_LOAD_IMAGE_COLOR :
		CV_LOAD_IMAGE_GRAYSCALE);
	cv::Mat cv_img_origin = cv::imread(filename, cv_read_flag);
	if (!cv_img_origin.data) {
		std::cerr << "Could not open or find file " << filename;
		return cv_img_origin;
	}
	if (height > 0 && width > 0) {
		cv::resize(cv_img_origin, cv_img, cv::Size(width, height));
	}
	else {
		cv_img = cv_img_origin;
	}
	return cv_img;
}

void ocall_PrepareBinaryImage(const char* filename, int height, int width, int is_color, const char* _encoding) {
	string _filename = ((string)filename) + ".binary";
	if (boost::filesystem::exists(_filename))
		return;
	string encoding(_encoding);
	cv::Mat cv_img = ReadImageToCVMat(filename, height, width, is_color);
	if (cv_img.data) {
		std::vector<uchar> buf;
		if (encoding.size() && !((cv_img.channels() == 3) == is_color && !height && !width && matchExt(filename, encoding)))
		{
			cv::imencode("." + encoding, cv_img, buf);
		}
		else
		{
			int datum_channels = cv_img.channels();
			int datum_height = cv_img.rows;
			int datum_width = cv_img.cols;
			buf.resize(datum_channels * datum_height * datum_width, 0);
			for (int h = 0; h < datum_height; ++h) {
				const uchar* ptr = cv_img.ptr<uchar>(h);
				int img_index = 0;
				for (int w = 0; w < datum_width; ++w)
					for (int c = 0; c < datum_channels; ++c) {
						int datum_index = (c * datum_height + h) * datum_width + w;
						buf[datum_index] = ptr[img_index++];
					}
			}
		}
		
		FILE *fp = fopen(_filename.c_str(), "wb");
		fwrite(&buf[0], sizeof(uchar), buf.size(), fp);
		fclose(fp);
	}
	else {
		std::cerr << "Failed to read image." << std::endl;
	}
}

//----------------------------------------------------------------------------------------------//

int NofMapping = 0;
unordered_map< int, std::pair<HANDLE, long> > Mapping_pool;

//TODO: mode
void ocall_open(int* fd, long long* ptr, long long* size, const char* filename, int mode) {
	HANDLE fileHandle = CreateFile(filename, GENERIC_READ, 0, 0, OPEN_ALWAYS, 0, 0);
	if (fileHandle != NULL)
	{
		*size = GetFileSize(fileHandle, NULL);
		HANDLE fileMapping = CreateFileMapping(fileHandle, 0, PAGE_READONLY, 0, 0, "CSFlash");
		CloseHandle(fileHandle);
		if (fileMapping)
		{
			*fd = NofMapping++;
			*ptr = (long long)MapViewOfFile(fileMapping, FILE_MAP_READ, 0, 0, 0);
			if (*ptr == 0)
			{
				DWORD ret = GetLastError();
				printf("Error No. = %d\n", ret);
			}
			Mapping_pool[*fd] = std::make_pair(fileMapping, *ptr);
		}
	}
}

void ocall_close(int fd) {
	UnmapViewOfFile((LPSTR)Mapping_pool[fd].second);
	CloseHandle(Mapping_pool[fd].first);
	Mapping_pool.erase(fd);
}
