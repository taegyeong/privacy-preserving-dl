#define USE_OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <caffe/caffe.hpp>

#include <algorithm>
#include <iostream>
#include <iosfwd>
#include <memory>
#include <utility>
#include <vector>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <string>
#include <unordered_map>

#include "PrivateAI_u.h"
#include "App.h"

using std::string;
using std::unordered_map;
using namespace caffe;

/* Pair (label, confidence) representing a prediction. */
typedef std::pair<int, float> Prediction;
std::vector<string> labels_;

class Classifier {
public:
	Classifier(const string& model_file,
		const string& trained_file,
		const string& mean_file,
		const string& label_file);

	std::vector<Prediction> Classify(const string image_file, int N = 5);
	shared_ptr<Net<float> > net_;

private:
	void SetMean(const string& mean_file);
	std::vector<float> Predict(const cv::Mat& img);
	std::vector<float> Predict(const string image_file);
	void WrapInputLayer(std::vector<cv::Mat>* input_channels);
	void Preprocess(const cv::Mat& img,
		std::vector<cv::Mat>* input_channels);

private:
	cv::Size input_geometry_;
	int num_channels_;
	cv::Mat mean_;
};

string i2s(int x)
{
	string y;
	stringstream f;
	f << x; f >> y;
	return y;
}

Classifier::Classifier(const string& model_file,
	const string& trained_file,
	const string& mean_file,
	const string& label_file) {
#ifdef CPU_ONLY
	Caffe::set_mode(Caffe::CPU);
#else
	Caffe::set_mode(Caffe::GPU);
#endif
	/* Load the network. */
	init_time_log();
	net_.reset(new Net<float>(model_file, TEST));
	record_time_log("Net create");
	net_->CopyTrainedLayersFrom(trained_file);
	record_time_log("Weight load");
	CHECK_EQ(net_->num_inputs(), 1) << "Network should have exactly one input.";
	CHECK_EQ(net_->num_outputs(), 1) << "Network should have exactly one output.";

	Blob<float>* input_layer = net_->input_blobs()[0];
	num_channels_ = input_layer->channels();
	CHECK(num_channels_ == 3 || num_channels_ == 1)
		<< "Input layer should have 1 or 3 channels.";
	input_geometry_ = cv::Size(input_layer->width(), input_layer->height());

	if (labels_.size() > 0)
		return;
	if (label_file != "")
	{
		/* Load labels. */
		std::ifstream labels(label_file.c_str());
		string line;
		while (std::getline(labels, line))
			labels_.push_back(string(line));

		Blob<float>* output_layer = net_->output_blobs()[0];
		CHECK_EQ(labels_.size(), output_layer->channels())
			<< "Number of labels is different from the output layer dimension.";
	}
	else
	{
		Blob<float>* output_layer = net_->output_blobs()[0];
		int num = output_layer->channels();
		for (int i = 0; i < num; ++i)
			labels_.push_back(i2s(i));
	}
}

static bool PairCompare(const std::pair<float, int>& lhs,
	const std::pair<float, int>& rhs) {
	return lhs.first > rhs.first;
}

/* Return the indices of the top N values of vector v. */
static std::vector<int> Argmax(const std::vector<float>& v, int N) {
	std::vector<std::pair<float, int> > pairs;
	for (size_t i = 0; i < v.size(); ++i)
		pairs.push_back(std::make_pair(v[i], i));
	std::partial_sort(pairs.begin(), pairs.begin() + N, pairs.end(), PairCompare);

	std::vector<int> result;
	for (int i = 0; i < N; ++i)
		result.push_back(pairs[i].second);
	return result;
}

/* Return the top N predictions. */
std::vector<Prediction> Classifier::Classify(const string image_file, int N) {
	std::vector<float> output = Predict(image_file);

	N = std::min<int>(labels_.size(), N);
	std::vector<int> maxN = Argmax(output, N);
	std::vector<Prediction> predictions;
	for (int i = 0; i < N; ++i) {
		int idx = maxN[i];
		//printf("Prediction[%d]: %d\n", i, idx);
		predictions.push_back(std::make_pair(idx, output[idx]));
	}
	return predictions;
}

/* Load the mean file in binaryproto format. */
void Classifier::SetMean(const string& mean_file) {
	BlobProto blob_proto;
	ReadProtoFromBinaryFileOrDie(mean_file.c_str(), &blob_proto);

	/* Convert from BlobProto to Blob<float> */
	Blob<float> mean_blob;
	mean_blob.FromProto(blob_proto);
	CHECK_EQ(mean_blob.channels(), num_channels_)
		<< "Number of channels of mean file doesn't match input layer.";

	/* The format of the mean file is planar 32-bit float BGR or grayscale. */
	std::vector<cv::Mat> channels;
	float* data = mean_blob.mutable_cpu_data();
	for (int i = 0; i < num_channels_; ++i) {
		/* Extract an individual channel. */
		cv::Mat channel(mean_blob.height(), mean_blob.width(), CV_32FC1, data);
		channels.push_back(channel);
		data += mean_blob.height() * mean_blob.width();
	}

	/* Merge the separate channels into a single image. */
	cv::Mat mean;
	cv::merge(channels, mean);

	/* Compute the global mean pixel value and create a mean image
	* filled with this value. */
	cv::Scalar channel_mean = cv::mean(mean);
	mean_ = cv::Mat(input_geometry_, mean.type(), channel_mean);
}

std::vector<float> Classifier::Predict(const cv::Mat& img) {
	Blob<float>* input_layer = net_->input_blobs()[0];
	input_layer->Reshape(1, num_channels_,
		input_geometry_.height, input_geometry_.width);
	/* Forward dimension change to all layers. */
	net_->Reshape();

	std::vector<cv::Mat> input_channels;
	WrapInputLayer(&input_channels);

	Preprocess(img, &input_channels);

	net_->Forward();

	/* Copy the output layer to a std::vector */
	Blob<float>* output_layer = net_->output_blobs()[0];
	const float* begin = output_layer->cpu_data();
	const float* end = begin + output_layer->channels();
	return std::vector<float>(begin, end);
}

/* Read image from file and predict */
std::vector<float> Classifier::Predict(const string image_file)
{
	init_time_log();
	Blob<float>* input_layer = net_->input_blobs()[0];
	int num_channels_ = input_layer->channels();
	int width_ = input_layer->width();
	int height_ = input_layer->height();

	Datum datum;
	if (!ReadImageToDatum(image_file, 0, height_, width_, num_channels_ == 3, &datum))
		std::cerr << "Error during file reading" << std::endl;
	Blob<float>* blob = new Blob<float>(1, datum.channels(), datum.height(), datum.width());

	BlobProto blob_proto;
	blob_proto.set_num(1);
	blob_proto.set_channels(datum.channels());
	blob_proto.set_height(datum.height());
	blob_proto.set_width(datum.width());
	int size_in_datum = std::max<int>(datum.data().size(),
		datum.float_data_size());

	for (int ii = 0; ii < size_in_datum; ++ii) {
		blob_proto.add_data(0.);
	}
	const string& data = datum.data();
	if (data.size() != 0) {
		for (int ii = 0; ii < size_in_datum; ++ii) {
			blob_proto.set_data(ii, blob_proto.data(ii) + (uint8_t)data[ii]);
		}
	}

	// Set data into blob
	blob->FromProto(blob_proto);

	// Fill the vector
	vector<Blob<float>*> bottom;
	bottom.push_back(blob);
	float type = 0.0;

	record_time_log("Image load");
	const vector<Blob<float>*>& result = net_->Forward(bottom, &type);
	record_time_log("Inference");

	/* Copy the output layer to a std::vector */
	Blob<float>* output_layer = net_->output_blobs()[0];
	const float* begin = output_layer->cpu_data();
	const float* end = begin + output_layer->channels();
	return std::vector<float>(begin, end);
}

/* Wrap the input layer of the network in separate cv::Mat objects
* (one per channel). This way we save one memcpy operation and we
* don't need to rely on cudaMemcpy2D. The last preprocessing
* operation will write the separate channels directly to the input
* layer. */
void Classifier::WrapInputLayer(std::vector<cv::Mat>* input_channels) {
	Blob<float>* input_layer = net_->input_blobs()[0];

	int width = input_layer->width();
	int height = input_layer->height();
	float* input_data = input_layer->mutable_cpu_data();
	for (int i = 0; i < input_layer->channels(); ++i) {
		cv::Mat channel(height, width, CV_32FC1, input_data);
		input_channels->push_back(channel);
		input_data += width * height;
	}
}

void Classifier::Preprocess(const cv::Mat& img,
	std::vector<cv::Mat>* input_channels) {
	/* Convert the input image to the input image format of the network. */
	cv::Mat sample;
	if (img.channels() == 3 && num_channels_ == 1)
		cv::cvtColor(img, sample, cv::COLOR_BGR2GRAY);
	else if (img.channels() == 4 && num_channels_ == 1)
		cv::cvtColor(img, sample, cv::COLOR_BGRA2GRAY);
	else if (img.channels() == 4 && num_channels_ == 3)
		cv::cvtColor(img, sample, cv::COLOR_BGRA2BGR);
	else if (img.channels() == 1 && num_channels_ == 3)
		cv::cvtColor(img, sample, cv::COLOR_GRAY2BGR);
	else
		sample = img;

	cv::Mat sample_resized;
	if (sample.size() != input_geometry_)
		cv::resize(sample, sample_resized, input_geometry_);
	else
		sample_resized = sample;

	cv::Mat sample_float;
	if (num_channels_ == 3)
		sample_resized.convertTo(sample_float, CV_32FC3);
	else
		sample_resized.convertTo(sample_float, CV_32FC1);

	cv::Mat sample_normalized;
	//cv::subtract(sample_float, mean_, sample_normalized);

	/* This operation will write the separate BGR planes directly to the
	* input layer of the network because it is wrapped by the cv::Mat
	* objects in input_channels. */
	cv::split(sample_float, *input_channels);

	CHECK(reinterpret_cast<float*>(input_channels->at(0).data)
		== net_->input_blobs()[0]->cpu_data())
		<< "Input channels are not wrapping the input layer of the network.";
}

LPCSTR ini_file = "..\\..\\config.ini";
struct infer_config {
	bool enclave;
	char model_name[256];
	char data_name[256];
	char hash_name[256];
	int trial;
	int verbose;
	bool image_input;
	char image_file[256];
};

void classify(bool enclave_enabled, Classifier classifier, string imagefile, int trial);
string get_profile_string(char* name, char* key) {
	char temp[256];
	GetPrivateProfileString(name, key, "", temp, 256, ini_file);
	return string(temp);
}
void print_prediction(int idx, float output) {
	if (idx > labels_.size())
		printf("Image is Classified as >> %5.3f: [%d]\n", output, idx);
	else
		printf("Image is Classified as >> %5.3f: [%d] %s\n", output, idx, labels_[idx].c_str());
}


void print_elapsed_time(const char* msg, int time) {
	printf("[%5d ms] %s\n", time, msg);
}

void test_model(infer_config cfg) {
	string model_file = get_profile_string(cfg.model_name, "model");
	string weight_file = get_profile_string(cfg.model_name, "weight");
	int image_mode = GetPrivateProfileInt(cfg.data_name, "image_mode", -1, ini_file);
	string image_file = get_profile_string(cfg.data_name, "image");
	string image_dir = get_profile_string(cfg.data_name, "image_dir");
	string label_file = get_profile_string(cfg.data_name, "label");
	int hash_function = GetPrivateProfileInt("hash", cfg.hash_name, -1, ini_file);

	if (cfg.image_input) {
		image_file = string(cfg.image_file);
		image_mode = 0;
	}

#if defined(_MSC_VER)
	if (query_sgx_status() < 0) {
		printf("SGX is disabled, or a reboot is required to enable SGX\n");
		return;
	}
#endif 
	/* Initialize the enclave */
	if (initialize_enclave() < 0) {
		printf("Failed to initialize the enclave\n");
		return;
	}

	if (cfg.verbose > 0) {
		std::cout
			<< std::string(50, '=') << std::endl
			<< "  ### SGX-Caffe for Windows "
#ifdef _DEBUG
			<< "(Debug)"
#endif
#ifdef EDEBUG
			<< "(Pre-release)"
#endif
			<< " ###" << std::endl
			<< std::string(50, '-') << std::endl
			<< "  * Model: " << cfg.model_name << std::endl;
		if (cfg.enclave)
			std::cout << "  * Memory: trusted (Enclave)" << std::endl;
		else
			std::cout << "  * Memory: untrusted (App)" << std::endl;
		std::cout << std::string(50, '=')
			<< std::endl;
	}

	// ------------------------------------------------------------------------------
	Classifier classifier(model_file, weight_file, "", label_file);
	vector<const float*> weight_ptr;
	if (cfg.enclave) {
		classifier.net_->ReshapeConvMMDiv();
		classifier.net_->collect_weight_ptr(weight_ptr, false);
		/* Caffe inside of Enclave */
		set_configs(global_eid, cfg.verbose);
		load_model(global_eid, model_file.c_str(), (long long)(&weight_ptr[0]), hash_function);
		// Test: weight data corruption
		//long long ppp = (long long) &weight_ptr[0];
		//**(float **)ppp = 0.1;
	}

	switch (image_mode) {
	case 0:
		classify(cfg.enclave, classifier, image_file, cfg.trial);
		break;
	case 1:
		// TODO: read files in image_dir
		break;
	default:
		std::cout << "Wrong image configuration (mode: -1)" << std::endl;
		break;
	}
	// ------------------------------------------------------------------------------
	/* Destroy the enclave */
	sgx_destroy_enclave(global_eid);
}

bool parse_argv(int argc, char **argv, infer_config* cfg);

/* Application entry */
int SGX_CDECL main(int argc, char **argv)
{
	infer_config cfg;
	char model_name[256];
	char data_name[256];
	char hash_name[256];
	
	cfg.enclave = GetPrivateProfileInt("config", "enclave", -1, ini_file) == 1;
	GetPrivateProfileString("config", "data", "", cfg.data_name, 256, ini_file);
	GetPrivateProfileString("config", "model", "", cfg.model_name, 256, ini_file);
	GetPrivateProfileString("config", "hash", "", cfg.hash_name, 256, ini_file);
	cfg.trial = GetPrivateProfileInt("config", "trial", 1, ini_file);
	cfg.verbose = GetPrivateProfileInt("config", "verbose", 1, ini_file);
	cfg.image_input = false;

	if (parse_argv(argc, argv, &cfg))
		test_model(cfg);
	else
		printf("Wrong arguments!\n");

	if (cfg.verbose > 0)
		print_time_log(cfg.trial, cfg.enclave);
	else {
		printf("%s,", cfg.image_file);
		print_time_key("Inference", cfg.trial, cfg.enclave);
	}

	if (argc < 2)
		system("pause");
	return 0;
}

void classify(bool enclave_enabled, Classifier classifier, string imagefile, int trial) {
	//printf("IMAGE FILE: %s\n", imagefile.c_str());
	if (enclave_enabled) {
		if (inference(global_eid, imagefile.c_str(), trial) != SGX_SUCCESS)
			abort();
	}
	else {
		std::vector<Prediction> predictions = classifier.Classify(imagefile);
		/* Print the top N predictions. */
		for (Prediction p : predictions) {
			print_prediction(p.first, p.second);
		}
	}
}

bool parse_argv(int argc, char **argv, infer_config* cfg) {
	if (argc < 2)
		return true;
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-m") == 0) {
			strcpy(cfg->model_name, argv[++i]);
		}
		else if (strcmp(argv[i], "-n") == 0) {
			cfg->trial = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-h") == 0) {
			strcpy(cfg->hash_name, argv[++i]);
		}
		else if (strcmp(argv[i], "-v") == 0) {
			cfg->verbose = atoi(argv[++i]);
		}
		else if (strcmp(argv[i], "-i") == 0) {
			strcpy(cfg->image_file, argv[++i]);
			cfg->image_input = true;
		}
		else return false;
	}
	return true;
}