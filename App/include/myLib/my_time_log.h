#ifndef MYTIMELOG_H
#define MYTIMELOG_H

#include <string>
#include <sstream>
#include <iostream>

using std::string;

void init_time_log(bool enclave = false);
void record_time_log(string message, bool enclave = false);
void print_time_log(int trial, bool enclave_only = false);
void print_time_key(string key, int trial, bool enclave = false);

/*

#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <map>

using std::string;
using std::vector;
using std::map;

void time_log_init();
void time_log_record(string message);
void time_log_print();

extern map<string, int> my_time_log;
extern int my_time_start;
//extern double GetTime();
inline void time_log_init() {
	//my_time_start = GetTime();
	my_time_start = 0;
}
inline void time_log_record(string message) {
	//int finish = GetTime();
	int finish = 0;
	if (my_time_log.count(message) == 0) {
		my_time_log.insert(make_pair(message, 0));
	}
	my_time_log[message] += finish - my_time_start;
	//my_time_start = GetTime();
	my_time_start = 0;
}
inline void time_log_print() {
	int time_total = 0;
	printf("  %s\n", string(30, '-').c_str());
	printf("   %-16s%12s\n", "Type", "Comp time(s)");
	printf("  %s\n", string(30, '-').c_str());
	for (auto iter = my_time_log.begin(); iter != my_time_log.end(); iter++) {
		printf("   %-16s%12.3f\n", iter->first.c_str(), iter->second / 1000.0);
		time_total += iter->second;
	}
	printf("  %s\n", string(30, '-').c_str());
	printf("   %-16s%12.3f\n", "Total", time_total / 1000.0);
	printf("  %s\n", string(30, '-').c_str());
}
*/

#endif
