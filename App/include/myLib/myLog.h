#ifndef MYLOG_H
#define MYLOG_H

#include <boost/shared_ptr.hpp>
using boost::shared_ptr;

#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <map>
using std::string;
using std::ostream;
using std::stringstream;
using std::vector;
using std::map;

#define DEBUG

#ifdef DEBUG
class myStream;
extern myStream myOut;
class myStream
{
public:
	template<typename T>
	myStream& operator << (T x)
	{
		return myOut;
	}
	myStream& operator << (ostream& (*op)(ostream&))
	{
		return myOut;
	}
};
#else
typedef stringstream myStream;
#endif

enum LOG_t {INFO=0, WARNING=1, gERROR=2, gFATAL_t=3, DISCARD=4};

myStream& LOG(LOG_t _i);

myStream& LOG_IF(LOG_t _i, bool _b);

myStream& LOG_EVERY_N(LOG_t _i, int _N);

myStream& DLOG(LOG_t _i);

myStream& CHECK(bool _b);

template <typename T>
inline myStream& CHECK(shared_ptr<T> _p)
{
	return CHECK(_p != NULL);
}

myStream& CHECK_EQ(int _a, int _b);

myStream& CHECK_EQ(string _a, string _b);

myStream& CHECK_NE(int _a, int _b);

myStream& CHECK_NE(void* _a, void* _b);

myStream& CHECK_GT(int _a, int _b);

myStream& CHECK_GE(int _a, int _b);

myStream& CHECK_LT(int _a, int _b);

myStream& CHECK_LE(int _a, int _b);

template <typename T>
inline T* CHECK_NOTNULL(T* _p)
{
	if (_p != NULL) return _p;
	CHECK(false) << "shared_ptr is NULL";
}

myStream& DCHECK(bool _b);

myStream& DCHECK_EQ(int _a, int _b);

myStream& DCHECK_NE(int _a, int _b);

myStream& DCHECK_GT(int _a, int _b);

myStream& DCHECK_GE(int _a, int _b);

myStream& DCHECK_LT(int _a, int _b);

myStream& DCHECK_LE(int _a, int _b);

#endif