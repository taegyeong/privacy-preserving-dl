#include "myLib/myLog.h"

#ifndef DEBUG
const int logN = 5;
const int checkN = 2;
myStream myLog[logN];
myStream myCheck[checkN];
#else
myStream myOut;
#endif

myStream& LOG(LOG_t _i)
{
#ifdef DEBUG
	return myOut;
#else
	return myLog[_i];
#endif
}

myStream& LOG_IF(LOG_t _i, bool _b)
{
#ifdef DEBUG
	return myOut;
#else
	if (_b) return myLog[_i];
	else return myLog[DISCARD];
#endif
}

myStream& LOG_EVERY_N(LOG_t _i, int _N)
{
#ifdef DEBUG
	return myOut;
#else
	static int cnt = 0;
	if (cnt++ % _N == 0) return myLog[_i];
	else return myLog[DISCARD];
#endif
}

myStream& DLOG(LOG_t _i)
{
#ifdef DEBUG
	return myOut;
#else
	return myLog[_i];
#endif
}

myStream& CHECK(bool _b)
{
#ifdef DEBUG
	return myOut;
#else
	return myCheck[_b];
#endif
}

myStream& CHECK_EQ(int _a, int _b)
{
	return CHECK(_a == _b);
}

myStream& CHECK_EQ(string _a, string _b)
{
	return CHECK(_a == _b);
}

myStream& CHECK_NE(int _a, int _b)
{
	return CHECK(_a != _b);
}

myStream& CHECK_NE(void* _a, void* _b)
{
	return CHECK(_a != _b);
}

myStream& CHECK_GT(int _a, int _b)
{
	return CHECK(_a > _b);
}

myStream& CHECK_GE(int _a, int _b)
{
	return CHECK(_a >= _b);
}

myStream& CHECK_LT(int _a, int _b)
{
	return CHECK(_a < _b);
}

myStream& CHECK_LE(int _a, int _b)
{
	return CHECK(_a <= _b);
}

myStream& DCHECK(bool _b)
{
	return CHECK(_b);
}

myStream& DCHECK_EQ(int _a, int _b)
{
	return CHECK(_a == _b);
}

myStream& DCHECK_NE(int _a, int _b)
{
	return CHECK(_a != _b);
}

myStream& DCHECK_GT(int _a, int _b)
{
	return CHECK(_a > _b);
}

myStream& DCHECK_GE(int _a, int _b)
{
	return CHECK(_a >= _b);
}

myStream& DCHECK_LT(int _a, int _b)
{
	return CHECK(_a < _b);
}

myStream& DCHECK_LE(int _a, int _b)
{
	return CHECK(_a <= _b);
}