#include "myLib\my_time_log.h"

#include <chrono>
#include <string>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>

using std::string;
using std::map;
using std::vector;
using time_point = std::chrono::time_point<std::chrono::high_resolution_clock>;
time_point app_start;
time_point enclave_start;
map<string, int> app_time_log;
map<string, int> enclave_time_log;

time_point current_time() {
	return std::chrono::high_resolution_clock::now();
}
int duration(time_point start, time_point finish) {
	return std::chrono::duration_cast<std::chrono::microseconds>(finish - start).count();
}

void init_time_log(bool enclave) {
	if (enclave)
		enclave_start = current_time();
	else
		app_start = current_time();
}
void record_app_time(string message) {
	time_point finish_t = current_time();
	if (app_time_log.count(message) == 0)
		app_time_log.insert(make_pair(message, 0));
	app_time_log[message] += duration(app_start, finish_t);
	app_start = current_time();
}
void record_enclave_time(string message) {
	time_point finish_t = current_time();
	if (enclave_time_log.count(message) == 0)
		enclave_time_log.insert(make_pair(message, 0));
	enclave_time_log[message] += duration(enclave_start, finish_t);
	enclave_start = current_time();
}
void record_time_log(string message, bool enclave) {
	if (enclave)
		record_enclave_time(message);
	else
		record_app_time(message);
}


typedef vector<string> str_v;
int table_width = 32;
#define MAX(A, B) A > B ? A : B

str_v export_table(map<string, int> logger, string title, int trial) {
	str_v table;
	if (logger.size() > 0) {
		int total = 0;
		char buff[256];
		sprintf(buff, "< %s (trial: %d) >", title.c_str(), trial);
		table.push_back(string(buff));
		table.push_back(string(table_width, '-'));
		sprintf(buff, " %-18s%12s ", "Type", "Duration(ms)");
		table.push_back(string(buff));
		table.push_back(string(table_width, '-'));

		for (auto iter = logger.begin(); iter != logger.end(); iter++) {
			sprintf(buff, " %-18s%12.3f ",
				iter->first.c_str(), iter->second / 1000.0 / trial);
			table.push_back(string(buff));
			total += iter->second;
		}
		if (logger.size() > 1) {
			table.push_back(string(table_width, '-'));
			sprintf(buff, " %-18s%12.3f ", "Total", total / 1000.0 / trial);
			table.push_back(string(buff));
		}
		table.push_back(string(table_width, '-'));
	}
	table.push_back(string(""));

	return table;
}

void print_time_log(int trial, bool enclave_only) {
	str_v u_table = export_table(app_time_log, "App Time Log", trial);
	str_v t_table = export_table(enclave_time_log, "Enclave Time Log", trial);
	const char* u_str;
	const char* t_str;
	if (enclave_only) {
		int row = t_table.size();
		for (int i = 0; i < row; i++) {
			if (i < t_table.size())
				t_str = t_table[i].c_str();
			printf("  %-32s\n", t_str);
		}
	}
	else
	{
		int row = MAX(u_table.size(), t_table.size());
		for (int i = 0; i < row; i++) {
			if (i < u_table.size())
				u_str = u_table[i].c_str();
			if (i < t_table.size())
				t_str = t_table[i].c_str();
			printf("  %-32s    %-32s\n", u_str, t_str);
		}
	}
}

void print_time_key(string key, int trial, bool enclave) {
	if (enclave)
		printf("%.3f\n", enclave_time_log[key] / 1000. / trial);
	else
		printf("%.3f\n", app_time_log[key] / 1000. / trial);
}





map<string, int> my_time_log;
int my_time_start;



//extern double GetTime();
//inline void time_log_init() {
//	my_time_start = GetTime();
//}
//inline void time_log_record(string message) {
//	int finish = GetTime();
//	if (my_time_log.count(message) == 0) {
//		my_time_log.insert(make_pair(message, 0));
//	}
//	my_time_log[message] += finish - my_time_start;
//	my_time_start = GetTime();
//}
//inline void time_log_print() {
//	int time_total = 0;
//	printf("  %s\n", string(30, '-').c_str());
//	printf("   %-16s%12s\n", "Type", "Comp time(s)");
//	printf("  %s\n", string(30, '-').c_str());
//	for (auto iter = my_time_log.begin(); iter != my_time_log.end(); iter++) {
//		printf("   %-16s%12.3f\n", iter->first.c_str(), iter->second / 1000.0);
//	}
//	printf("  %s\n", string(30, '-').c_str());
//	printf("   %-16s%12.3f\n", "Total", time_total / 1000.0);
//	printf("  %s\n", string(30, '-').c_str());
//}