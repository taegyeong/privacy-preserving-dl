# Privacy-preserving Deep Learning System

This repository contains an implementation of a privacy-preserving deep learning inference system for remote clouds or edge servers. The system prevents data disclosure and modification while inference from malicious attackers by leveraging trusted execution environment(TEE).

We modified Caffe, one of well-known deep learning framework, to port into Intel SGX enclaves. Since using shared libraries are not allowed in the enclaves, the repository includes all source files of Caffe and its dependecies.
The repository contains an implementation for Windows environment.

## Background

### Intel Software Guard Extension (SGX)

Intel SGX is a set of CPU instruction codes to allow developers to partition the application into TEE (called Enclaves) to protect the code and data from disclosure or modification.
In enclaves, system calls and dynamically linked libraries are not allowed.

* Homepage: https://software.intel.com/en-us/sgx
* Introduction: https://software.intel.com/en-us/sgx/details
* Tutorial: https://software.intel.com/en-us/articles/intel-software-guard-extensions-tutorial-part-1-foundation

### Caffe

* Homepage: https://caffe.berkeleyvision.org
* Repository: https://github.com/BVLC/caffe

## Getting Started

See [`INSTALL.md`](INSTALL.md) to install & run the project.

## Code Details

Refer to [`CODE_LAYOUT_WINDOWS.md`](CODE_LAYOUT_WINDOWS.md)

## Challenge & Approach

Since the enclave should be completely isolated from systems and other programs, system calls and dynamically linked libraries are not allowed in enclaves.
Caffe and its dependencies included in our system avoid to use all system calls and utilize ocall APIs provided from App.

The main challenge to run deep-learning models in the enclaves is limited resources: CPU only and lack of memory (128MB) which is too small to load pre-trained weights. Our system solves this challenge by loading models in untrusted zone since the models are non-sensitive data. Yet, loading models in unstrusted zone still have some problems.

### Memory-preserving Convolution

The enclave memory is still insufficient to compute convolutions. Im2col-based convolution lowering technique, which is commonly used in various frameworks including Caffe and TensorFlow, reduces convolution layer operation into a single matrix multiplication. It is very efficient in terms of computation speed with highly optimized matrix operation libraries such as OpenBLAS. However, it requires big memory overhead, and some convolution layers in VGG-16/19 requires more than 128MB to reshape the feature map.

We addressed this problem by splitting a single matrix multiplication into multiple MM. Our system divides kernels and reshaped featuremaps dynamically according to the requried size and gurantees that the convolution lowering will require memory within spefic size (currently 32MB).

### Weight Modification Detection

Loading models in untrusted zone generates another security problem that malicious attackers can modify the model to make wrong results.

The system adopted [xxHash](https://github.com/Cyan4973/xxHash), a non-cryptographic hash algorithm which has similar quality with MD5 but significantly faster than other hash functions. It precomputes the hash value of each layer's weight data and compare with recomputed ones during the inference.

### Copying Weight into Enclave

Since there still has a security hole: a time between hash checking and access, smart attackers can modify the weight just after the hash checking.

To make sure that our system can detect the weight modification, it copies the weights into Enclave before the hash checking. It reuses the memory to load copied weights, it is possible to copy the weight of each layer except fully-connected layers. For the fully-connected layers, we splitted the weights and computed via multiple matrix multiplications. 
