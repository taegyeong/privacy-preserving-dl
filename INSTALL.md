# SGX-Caffe Installation

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

* Visual Studio 2015
    - VS C/C++ compiler is required
* Intel SGX ([Guidelines for Windows](https://software.intel.com/en-us/articles/getting-started-with-sgx-sdk-for-windows))
    - Intel SGX SDK, PSW
    - 6th Generation(or higher) Intel Core processors
* OpenBLAS (Caffe dependency)
    - Clone & build from [OpenBLAS GitHub](https://github.com/xianyi/OpenBLAS)
    - Place `config.h` and `openblas_config.h` into `PrivateAI/include/cblas`
    - Include following C files into PrivateAI: `dgemm_incopy`, `dgemm_itcopy`, `sgemm_incopy`, `sgemm_itcopy`, `sgemm_oncopy`, `sgemm_otcopy` (you should find these files with `_$(ARCH).c` suffix)
    - Include following obj files: `sgemm_beta_$(ARCH)`, `sgemm_kernel_$(ARCH)` (these codes are implemented as assembler source which cannot be compiled with MSVC)

### Installation

Copy or clone `master` branch.

Run Visual Studio as Administrator and Open App & PrivateAI vcsproject with x64 platform.

* Set debugger as Intel(R) SGX Debugger and working directory as `$(OutDir)`.
* Check paths in projects' property settings:
    - Additional include directories (C/C++ -> General)
    - Additional library directories (Linker -> Genernal)
    - Additional dependencies (Linker -> input)
* Add OpenBLAS 
* Add preprocessor definitions:
    - App: `BOOST_ALL_NO_LIB`, `ENCLAVE`, `CPU_ONLY`, `USE_OPENCV`, `USE_LEVELDB`
    - PrivateAI: `ENCLAVE`, `CPU_ONLY`
* Locate dll files in `$(OutDir)` (used in App).

Modify `$(SGXSDKInstallPath)include\stdc++\win\typeinfo`: change `name()` in line 48 into public variable.

Path setting:

* Replace path to `config.ini` in `App/App.cpp`.
* Set path to protobuf & caffemodel files in `config.ini`.

Now you can build and run the solution!
